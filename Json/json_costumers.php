<?php
const MAIL_ALREADY_EXISTS = 1;
const USER_ALREADY_EXISTS = 2;
const SUCCESS_REGISTER = 0;

include '../Model/Model.php';
$model = new Model();
if (isset($_POST['username']) && isset($_POST['password'])) {
    $username = filter_var($_POST['username'], FILTER_SANITIZE_STRING);
    $password = md5($_POST['password']);
    $id = $model->get_costumer_id($username, $password);
    if ($id != -1 && $id) {
        session_start();
        $_SESSION['id'] = $id;
        $_SESSION['username'] = $username;
        header('Content-Type: application/json');
        echo json_encode(true);
    }
    else if($id == -1){
        echo json_encode(-1);
    }
    else{
        echo json_encode(false);
    }
} else 
if (isset($_GET['email']) && isset($_GET['username']) && isset($_GET['surname']) && isset($_GET['password']) && isset($_GET['name']) && isset($_GET['number'])) {
    $name = filter_var($_GET['name'], FILTER_SANITIZE_STRING);
    $surname = filter_var($_GET['surname'], FILTER_SANITIZE_STRING);
    $password = $_GET['password'];
    $email = filter_var($_GET['email'], FILTER_VALIDATE_EMAIL);
    $user = filter_var($_GET['username'], FILTER_SANITIZE_STRING);
    $number = filter_var($_GET['number'], FILTER_SANITIZE_NUMBER_INT);
    check_fields($name, $surname, $password, $email, $user, $number);
    $password = md5($password);
    $resultQueryEmail = $model->check_for_existing_mail($email);
    $resulQueryUsers = $model->check_for_existing_user($user);
    if($resulQueryUsers === -1 || $resultQueryEmail === -1){
        header('Content-Type: application/json');
        echo json_encode(-1);
        die;
    }
    $result = SUCCESS_REGISTER;
    if ($resulQueryUsers && $resultQueryEmail) {
        $result = MAIL_ALREADY_EXISTS + USER_ALREADY_EXISTS;
    } else if ($resulQueryUsers) {
        $result = USER_ALREADY_EXISTS;
    } else if ($resultQueryEmail) {
        $result = MAIL_ALREADY_EXISTS;
    } else {
        if ($number != -1) {
           $result = $model->insert_costumer_number($name, $surname, $email, $user, $password, $number);
        } else {
            $result = $model->insert_costumer($name, $surname, $email, $user, $password);
        }
    }
    header('Content-Type: application/json');
    echo json_encode($result);
}

function check_fields($name, $surname, $password, $email, $user, $number) {
    if (!(strlen($name) > 1 && strlen($surname) > 1 && strlen($password) > 7 && $email && $user && filter_var($number, FILTER_VALIDATE_INT))) {
       die;
    }
    if (!preg_match("^(?=\S{8,})(?=\S*[a-z])(?=\S*[A-Z])(?=\S*[\d])\S*$^", $password) ||
            !filter_var($email, FILTER_VALIDATE_EMAIL)) {
        die;
    }
}
