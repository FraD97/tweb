<?php
$number = $user->get_number();
$mail = $user->get_email();
?>

<div id="container_p">
    <form method="POST" id="form_announce" enctype="multipart/form-data" action="../Controller/Controller_announcement.php"> 
        <div id="title">CREA IL TUO ANNUNCIO</div>
        <div class="input-announce">
            <input id="title_input" name="title" class="form-control" type="text" placeholder="Inserisci un titolo" maxlength="30" >
            <div></div>
        </div>
        <div class="input-announce">
            <textarea name="description" class="form-control" rows="6" placeholder="Scrivi la recensione dell'oggetto da vendere" id="description_input" maxlength="200"></textarea>
            <div></div>
        </div>

        <div class="input-announce">
            <div class="custom-file">
                <input id="upload_input" type="file" name="file[]" class="custom-file-input" accept="image/*" multiple>
                <div></div>
                <label class="custom-file-label">Carica immagini (Massimo 3)</label>
            </div>
        </div>

        <div class="input-announce">
            <input name="price" id="price_input" class="form-control" placeholder="Prezzo" type="number">
            <div></div>
        </div>
        <?php
        if ($number) {
            ?>
            <div class="input-announce">
                <input name="number" id="number_input" value=<?=$number?> class="form-control" placeholder="Telefono" type="number">
                <div></div>
            </div>
            <?php
        } else {
            ?> 
            <div class="input-announce">
                <input name="number" id="number_input" class="form-control" placeholder="Telefono" type="number">
                <div></div>
            </div>
            <?php
        }
        ?>
        <div class="input-announce">
            <input name="mail" id="mail_input" value="<?= $mail ?>" class="form-control" placeholder="Mail" type="email">
            <div></div>
        </div>
        <div class="input-announce">
            <select  name="category" id="category_select" class="form-control">
                <option>Categoria</option>
                <option>Elettronica</option>
                <option>Automobili</option>
                <option>Videogiochi</option>
                <option>Telefonia</option>
                <option>Musica</option>
                <option>Sport</option>
                <option>Computer</option>
                <option>Per la Casa</option>
            </select>
            <div></div>
        </div>
        <div class="input-announce">
            <select name="region" id="region_select" class="form-control">
                <option>Regione</option>
                <option>Piemonte</option>
                <option>Val d'Aosta</option>
                <option>Lombardia</option>
                <option>Trentino Alto-Adige</option>
                <option>Friuli-Venezia Giulia</option>
                <option>Liguria</option>
                <option>Veneto</option>
                <option>Emilia-Romagna</option>
                <option>Toscana</option>
                <option>Marche</option>
                <option>Umbria</option>
                <option>Lazio</option>
                <option>Abruzzo</option>
                <option>Molise</option>
                <option>Campania</option>
                <option>Puglia</option>
                <option>Basilicata</option>
                <option>Calabria</option>
                <option>Sicilia</option>
                <option>Sardegna</option>
            </select>
            <div></div>
        </div>
        <div id="btn_container">
            <button id="btn_preview" type="button" class="btn btn-primary">Anteprima</button>
            <button type="button" class="btn btn-primary" id="btn_confirm">Conferma</button>
        </div> 
    </form>
</div>