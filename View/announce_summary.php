<?php
foreach ($announcement as $ann) {
    ?>
    <div class="announcement_container">
        <div class="image_announcement">
            <img src="<?='../img/announce/'.$ann->get_user_id().'A'.$ann->get_id().'_0" alt="immagine annuncio'?>" alt="immagine annuncio">
        </div>
        <div class="info_announcement">
            <div><?=$ann->get_title()?></div>
            <div class="username"><?=$this->get_username($ann->get_user_id())?></div>
            <div class="price"><?=$ann->get_price()?>€</div>
        </div>
        <div class="link_announcement">
            <div>
                <a class="link_view" href="<?='info_announcement.php?id='.$ann->get_id()?>">Visualizza</a>
            </div>
        </div>
    </div>
    <?php
}

