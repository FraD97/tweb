$(document).ready(function () {
    register();
    giveCategoryFeedBack();
    setButtonLinks();
});

function snackbarHandle() {
    var x = document.getElementById("snackbar");
    x.className = "show";
    setTimeout(function () {
        x.className = x.className.replace("show", "");
    }, 8000);
}


function register() {
    if ($("#snackbar").length) {
        snackbarHandle();
    }
}

function giveCategoryFeedBack() {
    $("#c1").add($("#c2")).add($("#c3")).add($("#c4")).add($("#c5")).add($("#c6")).add($("#c7")).add($("#c8")).add($("#c9")).hover(function () {
        switch (this.id) {
            case "c1":
                $("#c1 img").attr("src", "../img/camerawhite");
                break;
            case "c2":
                $("#c2 img").attr("src", "../img/carwhite");
                break;
            case "c3":
                $("#c3 img").attr("src", "../img/gamewhite");
                break;
            case "c4":
                $("#c4 img").attr("src", "../img/smartphonewhite");
                break;
            case "c5":
                $("#c5 img").attr("src", "../img/guitarwhite");
                break;
            case "c6":
                $("#c6 img").attr("src", "../img/ballwhite");
                break;
            case "c7":
                $("#c7 img").attr("src", "../img/macwhite");
                break;
            case "c8":
                $("#c8 img").attr("src", "../img/furniturewhite");
                break;
            case "c9":
                $("#c9 img").attr("src", "../img/allwhite");
                break;
        }

    }, function () {
        switch (this.id) {
            case "c1":
                $("#c1 img").attr("src", "../img/elettronica");
                break;
            case "c2":
                $("#c2 img").attr("src", "../img/car");
                break;
            case "c3":
                $("#c3 img").attr("src", "../img/game");
                break;
            case "c4":
                $("#c4 img").attr("src", "../img/smartphone");
                break;
            case "c5":
                $("#c5 img").attr("src", "../img/guitar");
                break;
            case "c6":
                $("#c6 img").attr("src", "../img/ball");
                break;
            case "c7":
                $("#c7 img").attr("src", "../img/mac");
                break;
            case "c8":
                $("#c8 img").attr("src", "../img/furniture");
                break;
            case "c9":
                $("#c9 img").attr("src", "../img/all");
                break;
        }
    });
}

function setButtonLinks() {
    $("#c1").add($("#c2")).add($("#c3")).add($("#c4")).add($("#c5")).add($("#c6")).add($("#c7")).add($("#c8")).add($("#c9")).click(function () {
        switch (this.id) {
            case "c1":
                window.location = "search.php?search_text=&region=all&category=Elettronica&page=0";
                break;
            case "c2":
                window.location = "search.php?search_text=&region=all&category=Automobili&page=0";
                break;
            case "c3":
                window.location = "search.php?search_text=&region=all&category=Videogiochi&page=0";
                break;
            case "c4":
                 window.location = "search.php?search_text=&region=all&category=Telefonia&page=0";
                break;
            case "c5":
                 window.location = "search.php?search_text=&region=all&category=Musica&page=0";
                break;
            case "c6":
                window.location = "search.php?search_text=&region=all&category=Sport&page=0";
                break;
            case "c7":
                window.location = "search.php?search_text=&region=all&category=Computer&page=0";
                break;
            case "c8":
                window.location = "search.php?search_text=&region=all&category=Per la Casa&page=0";
                break;
            case "c9":
                 window.location = "search.php?search_text=&region=all&category=all&page=0";
                break;
            }
    });
}