<?php

const MAX_ANNOUNCEMENTS_FOR_PAGE = 8;
include '../Model/Model.php';

class Controller_search {

    private $model;

    public function __construct() {
        $this->model = new Model();
    }

    public function invoke() {
        session_start();
        if (isset($_SESSION['id'])) {
            if (isset($_GET['search_text']) && isset($_GET['region']) && isset($_GET['category']) && isset($_GET['page'])) {
                $text = filter_var($_GET['search_text'], FILTER_SANITIZE_STRING);
                $region = filter_var($_GET['region'], FILTER_SANITIZE_STRING);
                $category = filter_var($_GET['category'], FILTER_SANITIZE_STRING);
                $page = filter_var($_GET['page'], FILTER_VALIDATE_INT);
                $announcements_data = $this->get_announcements($text, $category, $region, $page);
                $number_pages = ceil($announcements_data[1] / MAX_ANNOUNCEMENTS_FOR_PAGE);
                $announcement = $announcements_data[0];
                $n = $announcements_data[1];
                if ((($page >= $number_pages) && $n) || $page < 0 || ($n == 0 && $page > 0)) {
                    header("Location: ../src/error.php");
                    die;
                }
                include'../View/header.html';
                include'../View/searchbar.html';
                include'../View/search_inner.php';
                include'../View/footer.html';
            } else {
                header("Location: ../src/error.php");
                die;
            }
        } else {
            $this->redirect("login.php", "Effettua il login per accedere al sito");
        }
    }

    private function get_username($id) {
        return $this->model->get_username($id);
    }

    private function redirect($url, $flash_message = NULL) {
        if ($flash_message) {
            $_SESSION["flash"] = $flash_message;
        }
        header("Location: $url");
        die;
    }

    private function get_announces() {
        return $this->model->get_all_announcement();
    }

    private function get_announcements($text, $category, $region, $page) {
        $announcements_data = $this->model->get_announcement_searchbar($text, $category, $region, $page);
        if($announcements_data === null){
            header("Location: ../src/error.php?code=400");
            die;
        }
        return $announcements_data;
    }

}
