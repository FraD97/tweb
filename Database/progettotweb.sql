-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Creato il: Giu 22, 2019 alle 10:20
-- Versione del server: 5.7.23
-- Versione PHP: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `catering`
--
CREATE DATABASE IF NOT EXISTS `catering` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `catering`;

-- --------------------------------------------------------

--
-- Struttura della tabella `events`
--

DROP TABLE IF EXISTS `events`;
CREATE TABLE IF NOT EXISTS `events` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menu` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `events`
--

INSERT INTO `events` (`id`, `menu`) VALUES
(1, 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `menuitems`
--

DROP TABLE IF EXISTS `menuitems`;
CREATE TABLE IF NOT EXISTS `menuitems` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menu` int(11) DEFAULT NULL,
  `section` int(11) DEFAULT '0',
  `description` tinytext,
  `recipe` int(11) NOT NULL,
  `position` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `menuitems`
--

INSERT INTO `menuitems` (`id`, `menu`, `section`, `description`, `recipe`, `position`) VALUES
(1, 3, 1, 'Voce 1', 1, 0),
(2, 3, 1, 'Voce 2', 1, 1),
(3, 3, 2, 'Voce 3', 1, 0),
(4, 3, 2, 'Voce 4', 1, 2),
(5, 3, 0, 'Voce 0', 1, 0),
(6, 3, 1, 'Voce 1.5', 1, 2),
(7, 3, 2, 'Voce 3.5', 1, 1),
(8, 3, 0, 'Voce 0.2', 1, 1),
(9, 3, 0, 'Voce 0.8', 1, 2),
(10, 3, 3, 'Voce 5', 1, 0),
(11, 3, 3, 'Voce 6', 1, 1),
(12, 3, 3, 'Voce 7', 1, 2);

-- --------------------------------------------------------

--
-- Struttura della tabella `menus`
--

DROP TABLE IF EXISTS `menus`;
CREATE TABLE IF NOT EXISTS `menus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` tinytext NOT NULL,
  `menuowner` int(11) NOT NULL,
  `published` tinyint(1) DEFAULT NULL,
  `fingerFood` tinyint(1) DEFAULT NULL,
  `cookRequired` tinyint(1) DEFAULT NULL,
  `hotDishes` tinyint(1) DEFAULT NULL,
  `kitchenRequired` tinyint(1) DEFAULT NULL,
  `buffet` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `menus`
--

INSERT INTO `menus` (`id`, `title`, `menuowner`, `published`, `fingerFood`, `cookRequired`, `hotDishes`, `kitchenRequired`, `buffet`) VALUES
(1, 'prova in uso', 3, 1, 1, 0, 0, 0, 1),
(2, 'prova non in uso', 2, 0, 0, 1, 1, 1, 0),
(3, 'prova struttura', 3, 0, 0, 0, 0, 0, 1),
(4, 'Ciao', 3, 0, 0, 0, 0, 0, 0),
(7, 'prova struttura', 3, 0, 0, 0, 0, 0, 1),
(8, 'prova non in uso', 3, 0, 0, 1, 1, 1, 0);

-- --------------------------------------------------------

--
-- Struttura della tabella `recipes`
--

DROP TABLE IF EXISTS `recipes`;
CREATE TABLE IF NOT EXISTS `recipes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) DEFAULT NULL,
  `type` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `recipes`
--

INSERT INTO `recipes` (`id`, `name`, `type`) VALUES
(1, 'Salsa Tonnata', 'p'),
(2, 'Vitello Tonnato', 'r'),
(3, 'Vitello Tonnato all\'Antica', 'r'),
(4, 'Brodo di Manzo Ristretto', 'p'),
(5, 'Risotto alla Milanese', 'r'),
(6, 'Pesto Ligure', 'p'),
(7, 'Trofie avvantaggiate al pesto', 'r'),
(8, 'Orata al forno con olive', 'r'),
(9, 'Insalata russa', 'r'),
(10, 'Bagnet vert', 'p'),
(11, 'Acciughe al verde', 'r'),
(12, 'Agnolotti del plin', 'p'),
(13, 'Agnolotti al sugo d\'arrosto', 'r'),
(14, 'Agnolotti burro e salvia', 'r'),
(15, 'Brasato al barolo', 'r'),
(16, 'Panna cotta', 'r'),
(17, 'Tarte tatin', 'r');

-- --------------------------------------------------------

--
-- Struttura della tabella `roles`
--

DROP TABLE IF EXISTS `roles`;
CREATE TABLE IF NOT EXISTS `roles` (
  `id` varchar(1) NOT NULL,
  `role` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `roles`
--

INSERT INTO `roles` (`id`, `role`) VALUES
('c', 'Cuoco'),
('h', 'Chef'),
('o', 'Organizzatore'),
('s', 'Servizio');

-- --------------------------------------------------------

--
-- Struttura della tabella `sections`
--

DROP TABLE IF EXISTS `sections`;
CREATE TABLE IF NOT EXISTS `sections` (
  `menu` int(11) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` tinytext,
  `position` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `Sections_Menu_id_fk` (`menu`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `sections`
--

INSERT INTO `sections` (`menu`, `id`, `name`, `position`) VALUES
(3, 1, 'Primi', NULL),
(3, 2, 'Secondi', NULL),
(3, 3, 'Dessert', NULL);

-- --------------------------------------------------------

--
-- Struttura della tabella `userroles`
--

DROP TABLE IF EXISTS `userroles`;
CREATE TABLE IF NOT EXISTS `userroles` (
  `user` int(11) NOT NULL,
  `role` varchar(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `userroles`
--

INSERT INTO `userroles` (`user`, `role`) VALUES
(1, 'o'),
(2, 'h'),
(2, 'c'),
(3, 'h'),
(4, 'o'),
(4, 'h'),
(5, 'c');

-- --------------------------------------------------------

--
-- Struttura della tabella `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `users`
--

INSERT INTO `users` (`id`, `name`) VALUES
(1, 'Marco'),
(2, 'Tony'),
(3, 'Viola'),
(4, 'Anna'),
(5, 'Giovanni');
--
-- Database: `progettotweb`
--
CREATE DATABASE IF NOT EXISTS `progettotweb` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `progettotweb`;

-- --------------------------------------------------------

--
-- Struttura della tabella `announcement`
--

DROP TABLE IF EXISTS `announcement`;
CREATE TABLE IF NOT EXISTS `announcement` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `title` text NOT NULL,
  `description` text NOT NULL,
  `price` float NOT NULL,
  `number` varchar(12) DEFAULT NULL,
  `mail` varchar(50) NOT NULL,
  `category` varchar(20) NOT NULL,
  `region` varchar(30) NOT NULL,
  `time_announce` datetime NOT NULL,
  `state` varchar(10) NOT NULL DEFAULT 'active',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=125 DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `announcement`
--

INSERT INTO `announcement` (`id`, `user_id`, `title`, `description`, `price`, `number`, `mail`, `category`, `region`, `time_announce`, `state`) VALUES
(107, 31, 'Sony Playstation 4', 'La PlayStation 4 (abbreviata con la sigla PS4) Ã¨ una console per videogiochi creata dalla Sony Computer Entertainment, dotata di varie funzioni multimediali ', 299, '3472431784', 'fradisario18@gmail.com', 'Videogiochi', 'Piemonte', '2019-04-02 23:55:44', 'ended'),
(106, 43, 'Alfa Romeo MITO', 'L&#39;Alfa Romeo MiTo Ã¨ un&#39;automobile del segmento B prodotta dalla casa automobilistica italiana Alfa Romeo dal 2008 al 2018 nello stabilimento di Mirafiori.', 8900, '325648521', 'user@user.it', 'Automobili', 'Friuli-Venezia Giulia', '2019-04-02 23:54:44', 'active'),
(105, 43, 'Chitarra Acustica', 'Vendo chitarra acustica marca fender. Scambio a mano in zona Torino e provincia', 250, '325648521', 'user@user.it', 'Musica', 'Piemonte', '2019-04-02 23:43:42', 'active'),
(104, 43, 'Macbook pro', 'PiÃ¹ potenza. PiÃ¹ performance. PiÃ¹ pro. Compra ora. Scopri di piÃ¹. Chip Apple T2. Touch ID. Touch Bar. Display Retina. Tecnologia True Tone. Grafica strabiliante. ', 5000, '325648521', 'user@user.it', 'Computer', 'Piemonte', '2019-04-02 23:41:08', 'active'),
(103, 43, 'Iphone X', 'Apple iPhone X Ã¨ uno degli smartphone iOS piÃ¹ avanzati e completi che ci siano in circolazione. Dispone di un grande display da 5.8 pollici con una risoluzione Full HD+ ', 999, '325648521', 'user@user.it', 'Telefonia', 'Piemonte', '2019-04-02 23:38:22', 'active'),
(102, 43, 'Tesla Model S', 'Grazie al powertrain elettrico Tesla, garantisce un&#39;accelerazione senza precedenti ... Model S dispone di un sistema Autopilot studiato per rendere la guida piÃ¹  agevole e veloce', 100000, '325648521', 'user@user.it', 'Automobili', 'Piemonte', '2019-04-02 23:35:58', 'active'),
(100, 43, 'Tv 8k', 'Goditi il piacere di una RealtÃ  Pura ed Avvolgente con il Nuovo Samsung QLED 8K! Quantum Processor 8K. Funzione Ambient Mode. 100% di volume colore. 33 Milioni di Pixel. APP SmartThings. ', 50000, '325648521', 'user@user.it', 'Elettronica', 'Friuli-Venezia Giulia', '2019-04-02 23:32:41', 'active'),
(101, 43, 'Lenovo y730', 'Buy 15&#34; lightweight gaming laptop - Legion Y730 with GTX 1050 Ti graphics, NVIDIA Pascal GPU, optional G-Sync display, thin bezels, Intel Optane support.', 2500, '325648521', 'user@user.it', 'Computer', 'Piemonte', '2019-04-02 23:34:30', 'active'),
(99, 43, 'Sanbot v2', 'Sanbot Ã¨ progettato per portare l&#39;interazione tra uomo e macchina ad un livello nuovo e multi-sensoriale, il primo in Italia, distribuito da Omitech srl.', 9999, '325648521', 'user@user.it', 'Elettronica', 'Piemonte', '2019-04-02 23:30:38', 'active'),
(98, 43, 'Panca Piana', 'La panca piana veste un ruolo principale in palestra. Ma scopriamo pregi e difetti di questo esercizio per il gran pettorale per chi fa bodybuilding.', 54, '325648521', 'user@user.it', 'Sport', 'Piemonte', '2019-04-02 23:27:22', 'active'),
(97, 43, 'Aspirapolvere DYSON', 'Puoi dire addio al tuo aspirapolvere con filo. L&#39;ultima tecnologia nel campo dell&#39;aspirazione. Leggeri e potenti. Per la pulizia dal pavimento al soffitto.', 1524, '325648521', 'user@user.it', 'Per la Casa', 'Marche', '2019-04-02 23:25:49', 'active'),
(96, 43, 'Alfa Romeo 155', 'L&#39;Alfa Romeo 155 (numero di progetto 167) Ã¨ una berlina di segmento D costruita dalla casa automobilistica Alfa Romeo tra il 1992 e il 1998', 500, '325648521', 'user@user.it', 'Automobili', 'Abruzzo', '2019-04-02 23:24:50', 'active'),
(95, 43, 'Huawei p30', 'HUAWEI P30 sta spingendo oltre la Fotografia su smartphone. Scopri di piÃ¹! Tripla fotocamera Leica, Sensore Super Spectrum.', 999, '325648521', 'user@user.it', 'Telefonia', 'Puglia', '2019-04-02 23:23:43', 'active'),
(94, 42, 'God of War ps4', 'God of War per PS4. Ora che vive come un semplice mortale, lontano dall&#39;ombra degli dÃ¨i, Kratos deve adattarsi a terre sconosciute, inattesi pericoli.', 59, '6446616164', 'gastaFrinzi@libero.it', 'Videogiochi', 'Abruzzo', '2019-04-02 23:20:42', 'active'),
(93, 42, 'Chitarra elettrica', 'La chitarra elettrica Ã¨ un tipo di chitarra in cui la vibrazione delle corde viene rilevata da uno o piÃ¹ pick-up; il segnale viene quindi prelevato all&#39;uscita.', 100, '644661616', 'gastaFrinzi@libero.it', 'Musica', 'Campania', '2019-04-02 23:12:06', 'active'),
(92, 41, 'Hp omen 17', 'Mettiti alla prova e inizia la scalata verso la vittoria. Scopri tutta la gamma! Performance da urlo per dimostrare quanto vali. Pensato per i gamer. Potenza mostruosa.', 1999, '358962120', 'roberto.bellardi@edu.unito.it', 'Computer', 'Molise', '2019-04-02 23:03:08', 'active'),
(91, 40, 'Sanbot', 'Sanbot Ã¨ progettato per portare l&#39;interazione tra uomo e macchina ad un livello nuovo e multi-sensoriale, il primo in Italia, distribuito da Omitech srl.', 9999, '354266479', 'federico.fogola@unito.it', 'Elettronica', 'Molise', '2019-04-02 22:59:52', 'active'),
(90, 39, 'Monopattino Xiaomi', 'Compra Xiaomi Mi, Monopattino Elettrico Pieghevole, 30Km di autonomia, velocitÃ  fino a 25km/h, Nero, Unisex a prezzi vantaggiosi.', 250, '356862130', 'daniele@edu.unito.it', 'Elettronica', 'Sardegna', '2019-04-02 22:52:32', 'active'),
(89, 38, 'Gioconda originale', 'Vendo la gioconda, opera mia appena creata che diventerÃ  un successo ve lo posso assicurare. Prezzo non trattabile', 999999, '452135894', 'leonardo@gmail.com', 'Per la Casa', 'Lazio', '2019-04-02 22:50:31', 'active'),
(88, 37, 'Juventus FC', 'Vendo questa squadra, vorrei dedicarmi ad altro nel frattempo. Scambio a mano in zona Torino.', 999999, '3542589641', 'agnelli@libero.it', 'Sport', 'Piemonte', '2019-04-02 22:45:15', 'active'),
(86, 36, 'Apple Ipad Mini 5', 'Vendo Ipad Mini, come nuovo. Prezzo 600 euro. 50 caratteri sono troppi', 600, '3568621369', 'iginio@edu.unito.it', 'Elettronica', 'Lombardia', '2019-04-02 22:41:57', 'active'),
(87, 36, 'Sony Playstation 4', 'a PlayStation 4 (abbreviata con la sigla PS4) Ã¨ una console per videogiochi creata dalla Sony Computer Entertainment.', 299, '3568621369', 'iginio@edu.unito.it', 'Videogiochi', 'Basilicata', '2019-04-02 22:42:51', 'active'),
(85, 35, 'Marvel\'s Spiderman', 'Marvel&#39;s Spider-Man Ã¨ un videogioco d&#39;avventura dinamica sviluppato da Insomniac Games distribuito da Sony Interactive Entertainment.', 55, '3458546212', 'pippofranco@libero.it', 'Videogiochi', 'Sicilia', '2019-04-02 22:38:59', 'active'),
(84, 35, 'Alfa Romeo MITO', 'Vendo alfa romeo Mito. Come nuova solo 199999 kilometri fatti. Vendo a Torino e provincia.', 25000, '3458546212', 'pippofranco@libero.it', 'Automobili', 'Piemonte', '2019-04-02 22:38:02', 'active'),
(83, 34, 'Fender Stratocaster', 'Vendo chitarra elettrica Fender Stratocaster American modello 1999. Usata nei miei concerti. Valore inestimabile. Vendo in tutta Italia, scambio a mano in Sicilia', 100, '3549861494', 'richie.sambora@gmail.com', 'Musica', 'Sicilia', '2019-04-02 22:35:37', 'active'),
(82, 33, 'Chitarra Classica', 'Vendo chitarra Classica. Scambio a mano in tutta la sicilia! prezzo trattabile', 999, '3472454876', 'felice@gmail.com', 'Musica', 'Sicilia', '2019-04-02 22:27:21', 'active'),
(81, 32, 'Macbook pro', 'Vendo macbook pro. Usato pochissimo, prezzo non trattabile!', 1000, '3472457814', 'giuseppe@gmail.com', 'Computer', 'Piemonte', '2019-04-02 22:24:46', 'active'),
(79, 31, 'Nintendo Switch', 'Vendo Nintendo Switch Usato, ma come nuovo! Non legge soltanto i giochi, ma per il resto va una favola. Scambio a mano a Torino e provincia.', 199, '3472431784', 'fradisario18@gmail.com', 'Videogiochi', 'Piemonte', '2019-04-02 22:21:13', 'active'),
(80, 32, 'Samsung Galaxy s10', 'Vendo Samnsung Galaxy s10, come nuovo! Acquistato per errore, prezzo non trattabile. Scambio a Torino ma spedisco in tutta italia.', 100, '3472457814', 'giuseppe@gmail.com', 'Telefonia', 'Piemonte', '2019-04-02 22:23:26', 'active'),
(108, 42, 'Apple Ipad Mini 5', 'La PlayStation 4 (abbreviata con la sigla PS4) Ã¨ una console per videogiochi creata dalla Sony Computer Entertainment, dotata di varie funzioni multimediali ...', 9500, '64466161649', 'gastaFrinzi@libero.it', 'Elettronica', 'Piemonte', '2019-04-03 00:08:00', 'active'),
(109, 43, 'Samsung Galaxy s10', 'Samsung Galaxy S10 Ã¨ uno smartphone Android con caratteristiche all&#39;avanguardia che lo rendono una scelta eccellente per ogni tipo di utilizzo.', 900, '325648521', 'user@user.it', 'Telefonia', 'Piemonte', '2019-04-03 00:52:41', 'active'),
(124, 43, 'Apple airpods', 'Acquista i nuovi AirPods con custodia di ricarica wireless, usali con tutti i tuoi dispositivi. Spedizione gratis e incisione al laser, solo online.', 150, '325648521', 'user@user.it', 'Elettronica', 'Piemonte', '2019-06-22 11:44:04', 'active'),
(123, 43, 'Galaxy s10E come nuovo!', 'Samsung Galaxy S10e Ã¨ uno smartphone Android con caratteristiche all&#39;avanguardia che lo rendono una scelta eccellente per ogni tipo di utilizzo. Dispone di un grande display da 5.8 pollici.', 500, '325648521', 'user@user.it', 'Elettronica', 'Sicilia', '2019-06-20 23:21:38', 'active'),
(121, 31, 'Nintendo Switch', 'Nintendo SwitchNintendo SwitchNintendo SwitchNintendo SwitchNintendo SwitchNintendo SwitchNintendo SwitchNintendo Switch', 100, '3472431784', 'fradisario18@gmail.com', 'Automobili', 'Sicilia', '2019-05-31 22:51:48', 'ended');

-- --------------------------------------------------------

--
-- Struttura della tabella `category`
--

DROP TABLE IF EXISTS `category`;
CREATE TABLE IF NOT EXISTS `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text CHARACTER SET utf8,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `category`
--

INSERT INTO `category` (`id`, `name`) VALUES
(1, 'Elettronica'),
(3, 'Automobili'),
(4, 'Videogiochi'),
(5, 'Telefonia'),
(6, 'Musica'),
(7, 'Sport'),
(8, 'Computer'),
(9, 'Per la Casa');

-- --------------------------------------------------------

--
-- Struttura della tabella `costumer`
--

DROP TABLE IF EXISTS `costumer`;
CREATE TABLE IF NOT EXISTS `costumer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(15) NOT NULL,
  `surname` varchar(15) NOT NULL,
  `username` varchar(20) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `password` text,
  `number` varchar(12) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=53 DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `costumer`
--

INSERT INTO `costumer` (`id`, `name`, `surname`, `username`, `email`, `password`, `number`) VALUES
(31, 'Francesco', 'Di Sario', 'Feenix', 'fradisario18@gmail.com', '045635843ce3a230d859c2270daedba2', '3472431784'),
(32, 'Giuseppe', 'Ignone', 'Seppu97', 'giuseppe@gmail.com', '8cda12a70a34430ad06f9e05e7ac1c1d', '3472457814'),
(33, 'Felice', 'Cardone', 'Felice', 'felice@gmail.com', '9bad5a524de3e30b355f86a850e480c2', '3472454876'),
(34, 'Richie', 'Sambora', 'Sambo18', 'richie.sambora@gmail.com', '83e034a83c9101d18866867499680b73', '3549861494'),
(35, 'Pippo', 'franco', 'pippoFranco', 'pippofranco@libero.it', '03046b4ad783b22811281092b7a0dcae', '3458546212'),
(36, 'Iginio', 'Massari', 'IginioMassari', 'iginio@edu.unito.it', 'be444da7244c8c248af7fde62a95dd0b', '3568621369'),
(37, 'Andrea', 'Agnelli', 'Agnelli', 'agnelli@libero.it', '6e35d345ca73d2dc9d4a64d566dff444', '3542589641'),
(38, 'Leonardo', 'Da Vinci', 'LeoDaVinci', 'leonardo@gmail.com', '4490ca9bfa91db1a7b005490e42e65ca', '452135894'),
(39, 'Daniele', 'Camilleri', 'BlackMamba97', 'daniele@edu.unito.it', '83e034a83c9101d18866867499680b73', '356862130'),
(40, 'Federico', 'Fogola', 'Fogo', 'federico.fogola@unito.it', '83e034a83c9101d18866867499680b73', '354266479'),
(41, 'Roberto', 'Bellardi', 'Gioli1994', 'roberto.bellardi@edu.unito.it', '83e034a83c9101d18866867499680b73', '358962120'),
(42, 'Gastani', 'Frinzi', 'gastaFrinzi', 'gastaFrinzi@libero.it', '83e034a83c9101d18866867499680b73', '64466161649'),
(43, 'User', 'User', 'User', 'user@user.it', 'ef749ff9a048bad0dd80807fc49e1c0d', '325648521'),
(44, 'carlo', 'Di Sario', 'disaj', 'disaj@libero.it', 'fcfabafb2f1c913424f9ee1327245210', NULL),
(45, 'Alessia', 'Di ', 'aledisa', 'aledisa02@gmail.com', '1d543c259c49072817b038f385ea69b2', NULL),
(48, 'Peter', 'Parker', 'Spiderman', 'spiderman@gmail.com', '83e034a83c9101d18866867499680b73', NULL),
(49, 'Giuseppe', 'Conte', 'GConte', 'gconte@gmail.com', '045635843ce3a230d859c2270daedba2', '3472431785'),
(52, 'Francesco', 'Di Sario', 'PhoenixWright', 'fradisario180@gmail.com', 'ef749ff9a048bad0dd80807fc49e1c0d', '3472431784');

-- --------------------------------------------------------

--
-- Struttura della tabella `region`
--

DROP TABLE IF EXISTS `region`;
CREATE TABLE IF NOT EXISTS `region` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text CHARACTER SET utf8,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `region`
--

INSERT INTO `region` (`id`, `name`) VALUES
(1, 'Piemonte'),
(2, 'Val d\'Aosta'),
(3, 'Trentino Alto-Adige'),
(4, 'Friuli-Venezia Giulia'),
(5, 'Veneto'),
(6, 'Liguria'),
(7, 'Emilia-Romagna'),
(8, 'Toscana'),
(9, 'Marche'),
(10, 'Umbria'),
(11, 'Lazio'),
(12, 'Abruzzo'),
(13, 'Molise'),
(14, 'Campania'),
(15, 'Puglia'),
(16, 'Basilicata'),
(17, 'Calabria'),
(18, 'Sicilia'),
(19, 'Sardegna'),
(20, 'Lombardia');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
