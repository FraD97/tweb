<?php
class Controller_footer{
    
    public function invoke(){
        session_start();
        if(!isset($_SESSION['id'])){
            $this->redirect("login.php", "Effettua il login per accedere al sito");
        }
        if(isset($_GET['info']) && ($_GET['info'] == 1 || $_GET['info'] == 2)){
            include '../View/header.html';
            if($_GET['info'] == 1){
                include '../View/about_us.html';
            }
            else{
                include '../View/help.html';
            }
            include '../View/footer.html';
        }
        else{
            header('Location: error.php');
            die;
        }
    }
    
    private function redirect($url, $flash_message = NULL) {
        if ($flash_message) {
            $_SESSION["flash"] = $flash_message;
        }
        header("Location: $url");
        die;
    }
}

