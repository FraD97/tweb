<?php
/*
 * il seguente controller si occupa di controllare se l'utente ha il permesso di accedere alla pagina di creazione degli annunci
 */
include '../Model/Model.php';

class Controller_create_announcement {

    private $model;

    public function __construct() {
        $this->model = new Model();
    }

    public function invoke() {
        session_start();
        if (!isset($_SESSION['id'])) {
            $this->redirect("login.php", "Effettua il login per accedere al sito");
        } else {
            $user = $this->model->get_costumer_data($_SESSION['id']);
            if($user === false){
                header("Location: ../src/error.php?code=500");
            }
            if ($this->check_if_user_has_permissions()) {
                if ($user) {
                    include '../View/header.html';
                    include '../View/create_announcement_form.php';
                    include '../View/create_announcement_preview.html';
                    include '../View/footer.html';
                }
            } else {
                include '../View/header.html';
                include '../View/warning.html';
                include '../View/footer.html';
            }
        }
    }

    private function redirect($url, $flash_message = NULL) {
        if ($flash_message) {
            $_SESSION["flash"] = $flash_message;
        }
        header("Location: $url");
        die;
    }

    private function check_if_user_has_permissions() {
        $result = $this->model->get_last_announcement_time($_SESSION['id']);
        if($result === -1){
            header("Location: ../src/error.php?code=500");
            die;
        }
        return $result;
    }

}
