<!DOCTYPE html>
<?php
include '../Controller/Controller_rl.php';
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Effettua il Login</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
        <link href ="../Style/login.css" rel="stylesheet">
        <script src="../Javascript/login_page.js" type="text/javascript"></script>
    </head>
    <body>
        <?php
        $controller = new Controller_rl();
        $controller->invoke_l();
        ?>
    </body>
</html>
