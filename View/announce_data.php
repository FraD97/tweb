<div id="title_announce"><?=$announcement->get_title()?></div>
<div id="inner_container">
    <?php
    include 'carousel.php';
    ?>
    <div id="profile_container">
        <div id="info_profile">
            <div class="title">Dati Annuncio</div>
            <div class ="field_profile">Username: <span class="data"><?=$this->get_username($announcement->get_user_id())?></span></div>
            <div class ="field_profile">Mail: <span class="data"><?=$announcement->get_mail()?></span></div>
            <div class ="field_profile">Telefono: <span class="data"><?=$announcement->get_number()?></span></div>
            <div class ="field_profile">Regione: <span class="data"><?=$announcement->get_region()?></span></div>
            <div class ="field_profile">Categoria: <span class="data"><?=$announcement->get_category()?></span></div>
        </div>
    </div>
</div>
<div id="information_container">
    <div id="prezzo">
        <?=$announcement->get_price()?>€
    </div>
    <div id="descrizione">
        <?=$announcement->get_description()?>
    </div>
</div>