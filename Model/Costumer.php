<?php

/*
 * La seguente classe rappresenta un Cliente (Costumer) del sito.
 */

class Costumer implements JsonSerializable {

    private $id;
    private $name;
    private $surname;
    private $username;
    private $email;
    private $number;
    private $password;

    public function jsonSerialize() {
        return get_object_vars($this);
    }

    public function get_name() {
        return $this->name;
    }

    public function get_surname() {
        return $this->surname;
    }

    public function get_username() {
        return $this->username;
    }

    public function get_email() {
        return $this->email;
    }

    public function get_number() {
        return $this->number;
    }

}
