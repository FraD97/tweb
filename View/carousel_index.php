<?php
$number_item = intval($n / 3);
$k = 3;
if($number_item > 0){
?>
<div id="carousel" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner">
        <div class="carousel-item active">
            <div class = "recent">
                <div class="recent_container">
                    <a href="info_announcement.php?id=<?= $announcements[0]->get_id() ?>"><img src="../img/announce/<?= $announcements[0]->get_user_id() . 'A' . $announcements[0]->get_id() . '_0' ?>" class="card-img-top" alt="..."></a>
                    <div>
                        <div class="title_announce"><?= $announcements[0]->get_title() ?></div>
                        <div class="prezzo"><?= $announcements[0]->get_price() ?>€</div>
                    </div>
                </div>
                <div class="recent_container">
                    <a href="info_announcement.php?id=<?= $announcements[1]->get_id() ?>"><img src="../img/announce/<?= $announcements[1]->get_user_id() . 'A' . $announcements[1]->get_id() . '_0' ?>" class="card-img-top" alt="..."></a>
                    <div>
                        <div class="title_announce"><?= $announcements[1]->get_title() ?></div>
                        <div class="prezzo"><?= $announcements[1]->get_price() ?>€</div>
                    </div>
                </div>
                <div class="recent_container">
                    <a href="info_announcement.php?id=<?= $announcements[2]->get_id() ?>"><img src="../img/announce/<?= $announcements[2]->get_user_id() . 'A' . $announcements[2]->get_id() . '_0' ?>" class="card-img-top" alt="..."></a>
                    <div>
                        <div class="title_announce"><?= $announcements[2]->get_title() ?></div>
                        <div class="prezzo"><?= $announcements[2]->get_price() ?>€</div>
                    </div>
                </div>
            </div>
        </div>
        <?php
        for ($i = 1; $i < $number_item; $i++) {
            ?>
            <div class="carousel-item">
                <div class="recent">
                    <?php
                    for ($j = 0; $j < 3; $j++) {
                        ?>
                        <div class="recent_container">
                            <a href="info_announcement.php?id=<?= $announcements[$k]->get_id() ?>"><img src="../img/announce/<?= $announcements[$k]->get_user_id() . 'A' . $announcements[$k]->get_id() . '_0' ?>" class="card-img-top" alt="..."></a>
                            <div>
                                <div class="title_announce"><?= $announcements[$k]->get_title() ?></div>
                                <div class="prezzo"><?= $announcements[$k]->get_price() ?>€</div>
                            </div>
                        </div>
                        <?php
                        $k++;
                    }
                    ?>
                </div>
            </div>
            <?php
        }
        ?>
    </div>
    <a class="carousel-control-prev" href="#carousel" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carousel" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>
<?php
}?>