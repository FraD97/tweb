<div id="inner_container">
    <div class="card" style="width: 14rem;">
        <div id="img_container"><img src="../img/user.ico" class="card-img-top" alt="..."></div>
        <ul class="list-group list-group-flush">
            <li class="list-group-item">
                <h5>Dati personali</h5>
                <div><span class="field_profile">Nome: </span><?= $profile->get_name() ?></div>
                <div><span class="field_profile">Cognome: </span><?= $profile->get_surname() ?></div>
                <div><span class="field_profile">Username: </span><?= $profile->get_username() ?></div>
                <div><span class="field_profile">Email: </span><?= $profile->get_email() ?></div>
                <div><span class="field_profile">Telefono: </span> 3472431784</div>
            </li>
            <li class="list-group-item">
                <h5>Dati Annuncio</h5>
                <div><span class="field_profile">Annunci pubblicati: </span><span id="total_n_announcements"><?= $active_announcements + $ended_announcements ?></span></div>
                <div><span class="field_profile">Annunci attivi: </span><span id="active_n_announcements"><?= $active_announcements ?></span></div>
                <div><span class="field_profile">Annunci conclusi: </span><span id="ended_n_announcements"><?= $ended_announcements ?></span></div>
            </li>
        </ul>
    </div>
    <div id="column2">
        <?php
        include '../View/navbar.php';
        ?>
        <div id='container_active'>
        </div>

        <div id="container_ended">
        </div>
    </div>
</div>
<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modalTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalTitle">Rimuovi annuncio</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Vuoi davvero eliminare questo annuncio?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Annulla</button>
                <button type="button" class="btn btn-primary" id='btn_confirm'>Conferma</button>
            </div>
        </div>
    </div>
</div>