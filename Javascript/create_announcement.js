$(document).ready(function () {
    register();
    uploadSet();
});

function uploadSet() {
    var upload = $("#upload_input");
    upload.on("change", function () {
        if (upload[0].files.length <= 3 && upload[0].files.length > 0) {
            giveUserFeedback(upload, "Hai caricato " + upload[0].files.length + " immagini", 1);
            showMyImage(upload);
        } else if (upload[0].files.length > 3) {
            giveUserFeedback(upload, "Puoi caricare al massimo 3 immagini");
        } else {
            giveUserFeedback(upload, "Carica almeno una immagine");
        }
    });
}

function showMyImage(fileInput) {
    var files = fileInput[0].files;
    var j = 1;
    for (var i = 0; i < files.length; i++) {
        var file = files[i];
        var imageType = /image.*/;
        if (!file.type.match(imageType)) {
            continue;
        }
        var img = document.getElementById("img" + j);
        j++;
        img.file = file;
        var reader = new FileReader();
        reader.onload = (function (aImg) {
            return function (e) {
                aImg.src = e.target.result;
            };
        })(img);
        reader.readAsDataURL(file);
    }
}

function register() {
    var form = $("#form_announce");
    var title = $("#title_input");
    var feedbackTitle = $("#title_input + div");

    var description = $("#description_input");
    var feedbackDescription = $("#description_input + div");

    var upload = $("#upload_input");
    var feedbackUpload = $("#upload_input + div");

    var price = $("#price_input");
    var feedbackPrice = $("#price_input + div");

    var number = $("#number_input");
    var numberFeedback = $("#number_input + div");

    var mail = $("#mail_input");
    var mailFeedback = $("#mail_input + div");

    var categorySelect = $("#category_select");
    var feedbackCategory = $("#category_select + div");

    var regionSelect = $("#region_select");
    var feedbackRegion = $("#region_select + div");


    var btnConfirm = $("#btn_confirm");
    var btnPreview = $("#btn_preview");

    $("#modal").on("hidden.bs.modal", function () {
        btnPreview.removeAttr("data-toggle");
        btnPreview.removeAttr("data-target");
    });

    btnPreview.click(function () {
        if (checkValues(title, description, price, number, mail, categorySelect, regionSelect, upload)) {
            updateModal(title, description, price, number, mail, categorySelect, regionSelect, upload);
            btnPreview.attr("data-toggle", "modal");
            btnPreview.attr("data-target", ".bd-example-modal-lg");

        }
    });

    btnConfirm.click(function () {
        if (checkValues(title, description, price, number, mail, categorySelect, regionSelect, upload)) {
            form.submit();
        }
    });
}

function updateModal(title, description, price, number, mail, categorySelect, regionSelect, upload) {
    $("#title_announce").text(title.val());
    $("#descrizione").text(description.val());
    $("#prezzo").text(price.val());
    $("#telefono").text(number.val());
    $("#mail").text(mail.val());
    $("#category").text(categorySelect.val());
    $("#region").text(regionSelect.val());
}

function checkValues(title, description, price, number, mail, categorySelect, regionSelect, upload) {
    return checkTitle(title) && checkDescription(description) && checkUpload(upload) && checkPrice(price) && checkNumber(number) && checkMail(mail) && checkCategory(categorySelect) && checkRegion(regionSelect);
}

function checkUpload(upload) {
    if (upload[0].files.length <= 3 && upload[0].files.length > 0) {
        giveUserFeedback(upload, "I files sono validi", 1);
        return true;
    } else {
        giveUserFeedback(upload, "Carica almeno una foto riguardante l'annuncio");
        return false;
    }
}

function checkRegion(regionSelect) {
    if (regionSelect.val() === 'Regione') {
        giveUserFeedback(regionSelect, "Seleziona una Regione");
        return false;
    } else {
        giveUserFeedback(regionSelect, "Regione valida", 1);
        return true;
    }
}

function checkCategory(categorySelect) {
    if (categorySelect.val() === 'Categoria') {
        giveUserFeedback(categorySelect, "Seleziona una categoria");
        return false;
    } else {
        giveUserFeedback(categorySelect, "Categoria valida", 1);
        return true;
    }
}

function checkMail(mail) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (mail.val()) {
        if (re.test(mail.val())) {
            giveUserFeedback(mail, "Mail valida", 1);
            return true;
        } else {
            giveUserFeedback(mail, "Mail non valida");
            return false;
        }
    } else {
        giveUserFeedback(mail, "Per favore inserisci la tua Email");
        return false;
    }
}

function checkNumber(number) {
    if (!number.val().length) {
        giveUserFeedback(number, "Per favore inserisci un numero per contattarti");
        return false;
    } else if (number.val().lenght < 10 || number.val().length > 12) {
        giveUserFeedback(number, "Per favore inserisci un numero valido per contattarti");
        return false;
    } else {
        giveUserFeedback(number, "Numero valido", 1);
        return true;
    }
}

function checkPrice(price) {
    if (!price.val().length) {
        giveUserFeedback(price, "Per favore inserisci un prezzo (in euro)");
        return false;
    } else if (price.val() > 999999) {
        giveUserFeedback(price, "Non pensi di star esagerando?");
        return false;
    } else {
        giveUserFeedback(price, "Il prezzo è valido", 1);
        return true;
    }
}

function checkTitle(title) {
    if (!title.val().length) {
        giveUserFeedback(title, "Per favore inserisci un titolo di massimo 30 caratteri");
        return false;
    } else if (title.val().length >= 30) {
        giveUserFeedback(title, "Titolo troppo lungo (massimo 30 caratteri)");
        return false;
    } else {
        giveUserFeedback(title, "Il titolo è valido!", 1);
        return true;
    }
}

function checkDescription(description) {
    if (!description.val().length) {
        giveUserFeedback(description, "Per favore inserisci una descrizione di massimo 200 caratteri e minimo 50");
        return false;
    } else if (description.val().length > 200) {
        giveUserFeedback(description, "Descrizione troppo lunga (massimo 200 caratteri)");
        return false;
    } else if (description.val().length < 50) {
        giveUserFeedback(description, "Descrizione troppo corta (almeno 50 caratteri)");
        return false;
    } else {
        giveUserFeedback(description, "La descrizione è valida!", 1);
        return true;
    }
}

function giveUserFeedback(input, text, state) {
    var feedbackText = $("#" + input.attr("id") + " + div");
    feedbackText.html(text);
    if (state === 1) {
        input.removeClass("form-control is-invalid");
        input.addClass("form-control is-valid");
        feedbackText.attr("class", "valid-feedback");
    } else {
        input.addClass("form-control is-invalid");
        feedbackText.attr("class", "invalid-feedback");
    }
}
