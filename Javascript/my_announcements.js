var offsetA = 0;
var offsetE = 0;
var current_id = -1;

$(document).ready(function () {
    handleNavItem();
    registerClickDelete();
});


function handleNavItem() {
    var navActive = $("#nav_active");
    var navEnded = $("#nav_ended");
    var navFollowed = $("#nav_followed");
    var containerActive = $('#container_active');
    var containerEnded = $('#container_ended');

    navActive.click(function () {
        if (!navActive.hasClass('active')) {
            containerActive.show();
            navActive.addClass('active');
            hideOthers(navEnded, containerEnded);
            $("#show_a").show(); 
            $("#show_e").hide();
        }

    });
    navActive.click();
    navEnded.click(function () {
        if (!navEnded.hasClass('active')) {
            containerEnded.show();
            navEnded.addClass('active');
            hideOthers(navActive, containerActive);
            $("#show_a").hide();
            $("#show_e").show();
        }
    });
    requestAnnouncements(1, containerActive);
    requestAnnouncements(2, containerEnded, 1);
}


function hideOthers(navItem1, container1) {
    navItem1.removeClass('active');
    container1.hide();

}

function requestAnnouncements(option, container, flag) {
    if (option === 1) {
        $.ajax({
            type: "GET",
            dataType: "json",
            url: '../Json/json_announcement.php',
            data: {
                setting: option,
                offset: offsetA
            },
            success: function (result) {
                if(result){
                    addAnnouncements(result, container, flag);
                    handleOffsets(option, container);
                }
                else{
                    alert("La richiesta non può essere soddisfatti, dati non validi");
                }
            },
            error: function () {
                alert("Purtroppo la richiesta non può essere soddisfatta. Riprova più tardi");
            }
        });
    } else {
        $.ajax({
            type: "GET",
            dataType: "json",
            url: '../Json/json_announcement.php',
            data: {
                setting: option,
                offset: offsetE
            },
            success: function (result) {
                addAnnouncements(result, container, flag);
                handleOffsets(option, container);
                if (flag) {
                    container.hide();
                    $("#show_e").hide();
                }
            },
            error: function () {
                alert("Purtroppo la richiesta non può essere soddisfatta. Riprova più tardi");
            }
        });
    }
}

function handleOffsets(option, container) {
    if (option === 1) {
        offsetA += 6;
        if (offsetA < parseInt($('#active_n_announcements').text()) && !($("#show_a")).text()) {
            showMoreAnnouncementBtn(option, container, "show_a");
        } else if (offsetA >= parseInt($('#active_n_announcements').text())) {
            $("#show_a").remove();
        }
    } else if (option === 2) {
        offsetE += 6;
        if (offsetE < parseInt($('#ended_n_announcements').text()) && !($("#show_e")).text()) {
            showMoreAnnouncementBtn(option, container, "show_e");
        } else if (offsetE >= parseInt($('#ended_n_announcements').text())) {
            $("#show_e").remove();
        }
    }
}

function addAnnouncements(jsonAnnouncements, container, option) {
    if (!option) {
        for (i = 0; i < jsonAnnouncements.length; i++) {
            var src = "../img/announce/" + jsonAnnouncements[i].user_id + 'A' + jsonAnnouncements[i].id + '_0';
            container.append('<div class="announcement_container"><span class="id" hidden>' + jsonAnnouncements[i].id + '</span><div class="image_announcement"><img src="' + src + '" alt="immagine annuncio"></div><div class="info_announcement"><div>' + jsonAnnouncements[i].title + '</div><div class="price">' + jsonAnnouncements[i].price + '€</div></div><div class="link_announcement"><div><div><a class="link_view" href="info_announcement.php?id=' + jsonAnnouncements[i].id + '">Visualizza</a></div><button id="' + jsonAnnouncements[i].id + '" type="button" class="btn btn-link delete_announcement">Elimina</button></div></div></div>');
        }
    } else {
        for (i = 0; i < jsonAnnouncements.length; i++) {
            var src = "../img/announce/" + jsonAnnouncements[i].user_id + 'A' + jsonAnnouncements[i].id + '_0';
            container.append('<div class="announcement_container"><span class="id" hidden>' + jsonAnnouncements[i].id + '</span><div class="image_announcement"><img src="' + src + '" alt="immagine annuncio"></div><div class="info_announcement"><div>' + jsonAnnouncements[i].title + '</div><div class="price">' + jsonAnnouncements[i].price + '€</div></div><div class="link_announcement"><div><div><a class="link_view" href="info_announcement.php?id=' + jsonAnnouncements[i].id + '">Visualizza</a></div><button id="' + jsonAnnouncements[i].id + '"');
        }
    }
}

function showMoreAnnouncementBtn(option, container, id) {
    $("#column2").append('<button id=' + id + ' type="button" class="btn btn-link">Mostra ancora</button>');
    $('#' + id).click(function () {
        requestAnnouncements(option, container);
    });

}


function registerClickDelete() {
    $(document).on("click", '.delete_announcement', function () {
        current_id = this.id;
        $("#modal").modal('toggle');
    });
    $('#btn_confirm').click(function () {
        requestAnnouncementDeletion();
    });

}

function requestAnnouncementDeletion() {
    $.ajax({
        type: "POST",
        dataType: "json",
        url: '../Json/json_announcement.php',
        data: {
            announcement_id: current_id
        },
        success: function (result) {
            if (result) {
                location.reload();
            } else {
                alert("Non è possibile eliminare questo annuncio");
            }

        },
        error: function () {
            alert("Purtroppo la richiesta non può essere soddisfatta. Riprova più tardi");
        }
    });
}
