<!DOCTYPE html>
<?php
include '../Controller/Controller_my_announcements.php';
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title>I Miei Annunci</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="../style/header.css">
        <link rel="stylesheet" type="text/css" href="../style/common.css">
        <link rel="stylesheet" type="text/css" href="../style/footer.css">
        <link rel="stylesheet" type="text/css" href="../style/my_profile.css">
        <link rel="stylesheet" type="text/css" href="../style/announce.css">
        <script src="../Javascript/index.js" type="text/javascript"></script>
        <script src="../Javascript/menu.js" type="text/javascript"></script>
        <script src="../Javascript/my_announcements.js" type="text/javascript"></script>
    </head>
    <body>
        <?php
            $controller = new Controller_my_announcements();
            $controller->invoke();
        ?>
    </body>
</html>
