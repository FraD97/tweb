<?php
$pattern = $announcement->get_user_id() . 'A' . $announcement->get_id() . '_*';
$images_path = glob('../img/announce/' . $pattern);
?>
<div id="carousel_announce" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner">
        <div class="carousel-item active">
            <div class="carousel_image"><img class="d-block w-100" src="<?=$images_path[0]?>" alt="immagine annuncio"></div>
        </div>
        <?php
        for ($i = 1; $i < count($images_path); $i++) {
            ?>
            <div class="carousel-item">
                <div class="carousel_image"><img class="d-block w-100" src="<?=$images_path[$i]?>" alt="immagine annuncio"></div>
            </div>
            <?php
        }
        ?>
    </div>
    <a class="carousel-control-prev" href="#carousel_announce" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carousel_announce" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>