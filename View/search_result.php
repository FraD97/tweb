<?php
$text_t = $text;
$category_t = $category;
$region_t = $region;
if ($text == '') {
    $text_t = 'Tutto';
}

if ($category == 'all') {
    $category_t = 'Tutte le Categorie';
}
if ($region == 'all') {
    $region_t = 'Tutte le Regioni';
}
$actual_page = $page + 1;
$final_page = $number_pages;
?>
<div>
    Hai cercato: <span class="result_field"><?= $text_t ?></span> in <span class="result_field"><?= $region_t ?></span> nella Categoria: <span class="result_field"><?= $category_t ?></span>
    <?php
    if ($n == 0) {
        ?>
        <div id='no_announcement'>Nessun annuncio corrisponde ai criteri di ricerca</div>
        <?php
    } else {
        ?>
        <div id='page_number'>Pagina <?=$actual_page?> di <?= $number_pages ?></div>
        <?php
    }
    ?>
</div>
