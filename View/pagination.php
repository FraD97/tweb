<?php

function handle_first_page($number_pages) {
    echo '<li class="page-item active"><a class="page-link" href="http://localhost/Tweb/src/search.php?search_text=&region=all&category=all&page=1">1</a></li>';
    $end = 3;
    if($number_pages < 3){
        $end = $number_pages;
    }
    for ($i = 1; $i < $end; $i++) {
        echo '<li class="page-item"><a class="page-link" href="http://localhost/Tweb/src/search.php?search_text=&region=all&category=all&page=' . $i. '">' . ($i+1) . '</a></li>';
    }
    echo'</a></li><li class="page-item"><a class="page-link" href="http://localhost/Tweb/src/search.php?search_text=&region=all&category=all&page='.($number_pages - 1).'" aria-label="Next"><span aria-hidden="true">&raquo;</span></a></li>';
}

function handle_last_page($number_pages) {
    echo'<li class="page-item"><a class="page-link" href="http://localhost/Tweb/src/search.php?search_text=&region=all&category=all&page=0" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a>';
    $start = $number_pages - 2;
    if($start == 0){
        $start++;
    }
    for ($i = $start; $i < $number_pages; $i++) {
        echo '<li class="page-item"><a class="page-link" href="http://localhost/Tweb/src/search.php?search_text=&region=all&category=all&page=' . ($i-1) . '">' . $i . '</a></li>';
    }
    echo '<li class="page-item active"><a class="page-link" href="http://localhost/Tweb/src/search.php?search_text=&region=all&category=all&page=' . ($i-1) . '">' . $i . '</a></li>';
}

function handle_middle_page($actual_page, $number_pages) {
    echo '<li class="page-item"><a class="page-link" href="http://localhost/Tweb/src/search.php?search_text=&region=all&category=all&page=0" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a>'
    . '</li><li class="page-item"><a class="page-link" href="http://localhost/Tweb/src/search.php?search_text=&region=all&category=all&page=' . ($actual_page - 1) . '">' . ($actual_page) . '</a></li>';
        echo '<li class="page-item active"><a class="page-link" href="http://localhost/Tweb/src/search.php?search_text=&region=all&category=all&page=' . ($actual_page) . '">' . ($actual_page + 1) . '</a></li>';
            echo '<li class="page-item"><a class="page-link" href="http://localhost/Tweb/src/search.php?search_text=&region=all&category=all&page=' . ($actual_page + 1) . '">' . ($actual_page + 2) . '</a></li><li class="page-item"><a class="page-link" href="http://localhost/Tweb/src/search.php?search_text=&region=all&category=all&page='.($number_pages-1).'" aria-label="Next"><span aria-hidden="true">&raquo;</span></a></li>';
}
?>

<nav aria-label="Page navigation example">
    <ul class="pagination pagination-sm justify-content-end">
        <?php
        if ($page == 0) {
            handle_first_page($number_pages);
        } else if ($page == $number_pages - 1) {
            handle_last_page($number_pages);
        } else {
            handle_middle_page($page, $number_pages);
        }
        ?>
    </ul>
</nav>
